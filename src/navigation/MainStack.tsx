import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import React from "react";
import { Routes } from "./constants";
import { FontAwesome } from "@expo/vector-icons";
import Product from "../types/product";
import ScannerStack from './ScannerStack';
import SearchScreen from "../screens/Search/SearchScreen";

export type MainStackParamList = {
  [Routes.SCANNER]: undefined;
  [Routes.PRODUCT]: {product:Product};
  [Routes.SEARCH] : undefined;
};

const { Navigator: Stack, Screen } =
  createBottomTabNavigator<MainStackParamList>();

const MainStack = () => {
  return (
    <Stack>
      <Screen
        options={{
          tabBarIcon: () => (
            <FontAwesome name="quote-right" size={24} color="black" />
          ),
          tabBarLabel: "Scanner",
        }}
        name={Routes.SCANNER}
        component={ScannerStack}
      />
      <Screen
        options={{
          tabBarIcon: ({ focused }) => (
            <FontAwesome
              name="tasks"
              size={24}
              color={focused ? "red" : "black"}
            />
          ),
          tabBarLabel: "Search",
        }}
        name={Routes.SEARCH}
        component={SearchScreen}
      />
    </Stack>
  );
};

export default MainStack;
