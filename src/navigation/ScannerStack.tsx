import React from "react";
import { Routes } from "./constants";
import ProductScreen from "../screens/Scanner/ProductScreen";
import Product from "../types/product";
import { createStackNavigator } from "@react-navigation/stack";
import ScannerScreen from "../screens/Scanner/ScannerScreen";

export type ScannerStackParamList = {
  [Routes.SCANNER]: undefined;
  [Routes.PRODUCT]: {product:Product};
};

const Stack = createStackNavigator();

const ScannerStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name={Routes.SCANNER}
        component={ScannerScreen}
      />
      <Stack.Screen
        name={Routes.PRODUCT}
        component={ProductScreen}
      />
    </Stack.Navigator>
  );
};

export default ScannerStack;
