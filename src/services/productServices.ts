import axios from "axios";
import Product from '../types/product';
import ProductOnly from '../types/productOnly';

// axios.defaults.headers.common = {
//   "User-Agent": "SimplonSchoolTraining - Android - Version 1.0 - https://gitlab.com/samuelkeller",
// };

export const getProductInfo = async (barcode:string) => {
  try {
    const response = await axios.get<any>('https://world.openfoodfacts.org/api/v0/product/' + barcode);
    const product = response.data
    return product;
  } catch (error) {
    console.error(error);
  }
}

export const getProductInfoByName = async (name:string):Promise<ProductOnly[]> =>  {
  try {
    const response = await axios.get <any>(`https://world.openfoodfacts.org/cgi/search.pl?search_terms=${name}&search_simple=1&action=process&page_size=20&json=1`)
    const products:ProductOnly[] = response.data.products
    return products;
  } catch (error) {
    throw error;
  }
} 
