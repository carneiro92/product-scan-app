import React from "react";
import { Image } from "react-native";
import { StyleSheet, Text, View,TouchableOpacity } from "react-native";
import ProductOnly from "../../types/productOnly";
import { useNavigation } from '@react-navigation/native';

interface props {
  data: ProductOnly;
}

const SearchedProductList = (props: props) => {
  const { navigate } = useNavigation();
  return (
    <View>
    {props.data && props.data.product_name ? (
    <TouchableOpacity style={styles.container} onPress={()=> console.log(props.data)}>
      <Image
        style={styles.image}
        source={{ uri: props.data.image_thumb_url }}
      ></Image>
      
        <View style={styles.text}>
          <Text>Product Name: {props.data.product_name}</Text>
          <Text>
            Origin : {props.data.origins ? props.data.origins : "Pas d'information"}
          </Text>
          <Text>Quantity : {props.data.quantity}</Text>
        </View>
      
    </TouchableOpacity>
    ) : null}
    </View>
  );
};

export default SearchedProductList;

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    margin: 5,
    alignItems: "center",
    borderRadius: 20,
    backgroundColor: "lightgray",
    height: 100,
  },
  image: {
    marginLeft: 10,
    width: 50,
    height: 50,
  },
  text: {
    marginLeft: 20,
  },
});
