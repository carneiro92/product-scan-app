import { useNavigation } from '@react-navigation/native';
import React, { useState } from 'react'
import { StyleSheet, Text, View, SafeAreaView, TextInput, Button } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import { getProductInfoByName } from '../../services/productServices';
import ProductOnly from '../../types/productOnly';
import SearchedProductList from './SearchedProductList';

interface Props {
  
}

const SearchScreen = (props: Props) => {
  
  const [productName, onChangeProductName] = useState("")
  const { navigate } = useNavigation();
  const [productData, setProductData] = useState<ProductOnly[]>([])

  const handleForm = async () => {
    const data = await getProductInfoByName(productName)   
    setProductData(data)
    // navigate("Product", { product : productData});
  }

  return (
    <SafeAreaView style={styles.container}>
      <View>
        <Text style={styles.title}>Rechercher un Produit</Text>
      </View>
      <View style={styles.inputContainer}>
        <TextInput style={styles.input}
        onChangeText={onChangeProductName}
        placeholder="Entrez le nom du produit"
        value={productName} />
      </View>
      <View style={styles.formButton}>
        <Button title="Rechercher" onPress={handleForm}/>
      </View>
      <View style={styles.scrollContainer}>
        {productData.length > 1 && <FlatList data={productData} renderItem={({item } : {item:ProductOnly})=>(<SearchedProductList data={item} />)
}/>}
      </View>
    </SafeAreaView>
  )
}

export default SearchScreen

const styles = StyleSheet.create({
  container : {
    flex: 1,
    flexDirection:"column"
  },
  title: {
    marginTop:50,
    fontSize: 25,
    fontStyle: "italic",
    margin: 10,
    textAlign:"center"
  },
  input:{
    minWidth:300,
    textAlign:'center',
    fontSize:20,
    borderColor:"gray",
    borderStyle:'solid',
    borderBottomWidth:1
  },
  inputContainer:{
    marginTop:50,
    alignItems:'center',
  },
  formButton:{
    marginTop:20,
  },
  scrollContainer: {
    flex:1
  }

})
