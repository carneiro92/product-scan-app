import { RouteProp, useRoute } from "@react-navigation/native";
import React from "react";
import {
  Image,
  StyleSheet,
  Text,
  View,
} from "react-native";
import { MainStackParamList } from "../../navigation/MainStack";
import { Routes } from '../../navigation/constants';

type ProductScreenRouteProps = RouteProp<MainStackParamList,Routes.PRODUCT>


const ProductScreen = () => {
  
  const {params}= useRoute<ProductScreenRouteProps>()
  
  if (params?.product === undefined) {
    return (
      <View style={styles.container}>
        <Text style={styles.title}>Pas de produit scanné</Text>
      </View>
      )
    
  } else {
    const product = params?.product;
    return (
      <View style={styles.container}>
        <Image
          style={styles.image}
          source={{ uri: product.product.image_url }}
        ></Image>
        <View style={styles.containerDesc}>
          <Text style={styles.title}>{product.product.product_name}</Text>
          <Text style={styles.desc}>Ingredients : {product.product.ingredients_text}</Text>
          <Text style={styles.desc}>Nutritional Score Grade: {product.product.nutriscore_grade}</Text>
          <Text style={styles.desc}>Nutritional Score : {product.product.nutriscore_score}</Text>
          <Text style={styles.desc}>Origin : {product.product.origins}</Text>
          <Text style={styles.desc}>Quantity : {product.product.quantity}</Text>
        </View>
      </View>
    );
  }
};

export default ProductScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    alignItems: "center",
  },
  image: {
    width: 250,
    height: 250,
  },
  title: {
    fontSize: 25,
    fontStyle: "italic",
    margin: 10,
    textAlign:"center"
  },
  subtitle: {
    fontSize: 25,
    fontStyle: "italic",
    margin: 5,
  },
  containerDesc:{
    flex:0.8,
    width:"90%",
    borderRadius:20,
    margin:20,
    backgroundColor: 'lightgray',
    overflow:"scroll"
  },
  desc:{
    margin:10,
    fontSize:15
  }
});
